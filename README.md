# README #

This is a Java program which assists me in trading on the Bitcoin stock market.
The application was designed to be modular and expandable.


Features:


• Supports the exchanges Poloniex and Bitfinex by using their HTTP REST API. Expandable to more exchanges

• Grabs the graphs by opening up a web browser window, going to the exchange's website, and taking a screenshot. Uses Google Chrome for Java API 

• A "trigger" system which e-mails the user when certain conditions happen in the market

• Detects market conditions with charting tools. Includes simple/exponential moving averages, volatility measurements, and trendlines. Upgradable to more charting tools

• Uses a MySQL database to record messages, trades, market transactions

• Support for a complementary Android application


Sample:


![5a9c2db2ed8445b7b1793c2b450cfe68.png](https://bitbucket.org/repo/nMK9ko/images/4025137290-5a9c2db2ed8445b7b1793c2b450cfe68.png)