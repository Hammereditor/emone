package marketmanagers;

import eu.verdelhan.ta4j.Rule;
import eu.verdelhan.ta4j.Strategy;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.trackers.EMAIndicator;
import eu.verdelhan.ta4j.trading.rules.CrossedDownIndicatorRule;
import eu.verdelhan.ta4j.trading.rules.CrossedUpIndicatorRule;
import livetrading.DatabaseMarketTickProvider;
import livetrading.MarketManager;
import livetrading.MarketManagerConfig;
import livetrading.MarketTickProvider;
import livetrading.MovingStrategyTickManager;

public class BtcVtcManager extends MarketManager
{
	public BtcVtcManager(MarketManagerConfig config) throws Exception 
	{
		super(config);
	}

	@Override
	protected MarketTickProvider getMarketTickProvider(MovingStrategyTickManager mstm, MarketManagerConfig config) 
	{
		return new DatabaseMarketTickProvider(mstm, config.getTickTimeS(), config.getMarketID());
	}

	@Override
	protected int getStoredTicks() 
	{
		return 9;
	}

	@Override
	protected Strategy generateStrategy(TimeSeries series) 
	{
		ClosePriceIndicator closePrice = new ClosePriceIndicator(series);
		EMAIndicator shortEma = new EMAIndicator(closePrice, 9);
		EMAIndicator longEma = new EMAIndicator(closePrice, 3);
		
		Rule buyingRule = new CrossedUpIndicatorRule(shortEma, longEma);
		Rule sellingRule = new CrossedDownIndicatorRule(shortEma, longEma);
		return new Strategy(buyingRule, sellingRule);
	}

}
