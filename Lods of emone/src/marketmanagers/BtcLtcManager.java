package marketmanagers;

import eu.verdelhan.ta4j.Indicator;
import eu.verdelhan.ta4j.Rule;
import eu.verdelhan.ta4j.Strategy;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.indicators.simple.ClosePriceIndicator;
import eu.verdelhan.ta4j.indicators.trackers.EMAIndicator;
import eu.verdelhan.ta4j.trading.rules.CrossedDownIndicatorRule;
import eu.verdelhan.ta4j.trading.rules.CrossedUpIndicatorRule;
import livetrading.DatabaseMarketTickProvider;
import livetrading.MarketManager;
import livetrading.MarketManagerConfig;
import livetrading.MarketTickProvider;
import livetrading.MovingStrategyTickManager;

public class BtcLtcManager extends MarketManager
{
	public BtcLtcManager(MarketManagerConfig config) throws Exception 
	{
		super(config);
	}

	@Override
	protected MarketTickProvider getMarketTickProvider(MovingStrategyTickManager mstm, MarketManagerConfig config) 
	{
		return new DatabaseMarketTickProvider(mstm, config.getTickTimeS(), config.getMarketID());
	}

	@Override
	protected int getStoredTicks() 
	{
		return 9;
	}

	@Override
	protected Strategy generateStrategy(TimeSeries series) 
	{
		ClosePriceIndicator closePrice = new ClosePriceIndicator(series);
		Indicator mediumMA = new EMAIndicator(closePrice, 70);
		Indicator shortMA = new EMAIndicator(closePrice, 10);

		Rule buyingRule = new CrossedUpIndicatorRule(shortMA, mediumMA);
		Rule sellingRule = new CrossedDownIndicatorRule(shortMA, mediumMA);
		return new Strategy(buyingRule, sellingRule);
	}

}
