package livetrading;

import tradingobjs.Reinvestor;

public class TestReinvestor extends Reinvestor
{
	public double getReinvestVolume(double affordableVolumeWithoutFee, double pricePerCoin)
	{
		//return 0.00001 BTC or (100% - fee), whichever is lower
		//double fixedCoinVolume = 0.00001 / pricePerCoin;
		double fixedCoinVolume = 0.01 / pricePerCoin;
		double adjustableVolume = affordableVolumeWithoutFee * (1 - Reinvestor.TRADINGFEE);
		
		if (fixedCoinVolume < adjustableVolume)
			return fixedCoinVolume;
		else
			return adjustableVolume;
	}
}
