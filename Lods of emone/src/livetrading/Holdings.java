package livetrading;

import livetrading.Cryptsy.Balances;
import livetrading.Cryptsy.PublicMarket;

public class Holdings 
{
	private Cryptsy account;
	private double baseCurrencyBalance; //BTC, LTC, XRP ...
	private double tradedCurrencyBalance; //DOGE, VTC, LTC, GLD ...
	private PublicMarket marketInfo;
	
	public Holdings(Cryptsy account, int marketID) throws Exception
	{
		this.account = account;
		try { marketInfo = account.getPublicMarketData(marketID); } catch (Exception e) {
			throw new Exception("Error while getting market info", e);
		}
		try { refreshHoldings(); } catch (Exception e) {
			throw new Exception("Error while initially refreshing balance", e);
		}
	}
	
	public void refreshHoldings() throws Exception
	{
		Balances b = account.getInfo().balances_available;
		baseCurrencyBalance = b.get(marketInfo.primarycode);
		tradedCurrencyBalance = b.get(marketInfo.secondarycode);
	}
	
	public double getBaseCurrencyBalance() {
		return baseCurrencyBalance;
	}

	public double getTradedCurrencyBalance() {
		return tradedCurrencyBalance;
	}
}
