package livetrading;

import java.util.List;

import livetrading.Cryptsy.Trade;
import tradingobjs.OrderbookOrder;
import config.Logs;
import eu.verdelhan.ta4j.Decimal;
import eu.verdelhan.ta4j.Strategy;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;
import eu.verdelhan.ta4j.TradingRecord;
import eu.verdelhan.ta4j.analysis.CashFlow;

/**
 * Brings together the market ticks and strategy, in order to make ordering decisions
 * This class makes the raw decisions, and has nothing to do with quantities.
 * @author Hammereditor
 *
 */
public class MovingStrategyTickManager 
{
	private Strategy strategy;
	private TimeSeries previousPrices;
	private TradingRecord previousActions;
	private Broker broker;
	private CashFlow profitAnalyzer;
	
	private static final boolean forReal = false;
	
	/**
	 * Creates a new MSTM based on a strategy and the last few series of prices. Assumes no trades were done so far.
	 * @param strategy The trading strategy
	 * @param previousPrices The series of previous prices, which must be full
	 * @param storedTicks The max. number of ticks to store
	 * @param broker The object which executes the orders
	 */
	public MovingStrategyTickManager(Strategy strategy, TimeSeries previousPrices, int storedTicks, Broker broker)
	{
		this.strategy = strategy;
		this.previousPrices = previousPrices;
		this.broker = broker;
		this.previousActions = new TradingRecord();
		
		previousPrices.setMaximumTickCount(storedTicks);
		profitAnalyzer = new CashFlow(previousPrices, previousActions);
		Logs.debug("MovingStrategyTickManager(): constructed.");
	}
	
	public boolean coinsHeld()
	{
		return !previousActions.isClosed();
	}
	
	public double getNextOrderPrice() throws Exception
	{
		if (coinsHeld())
			return broker.getBestBuyOrder().getPricePerCoin();
		else
			return broker.getBestSellOrder().getPricePerCoin();
	}
	
	/**
	 * Makes a decision when a new tick starts. For example, the time is 6000, the storedTicks is 10, and the tick length is 300. This function will make a decision based on the data 
	 * of times 3000-6000.
	 * @param t The tick which just ended
	 * The decision: 0 = nothing, 1 = buy, 2 = sell
	 */
	public void decideAfterTick(Tick t) throws Exception
	{
		previousPrices.addTick(t);
		int endInd = previousPrices.getEnd();
		int decision = 0;
		Logs.info("MovingStrategyTickManager.decideAfterTick(): a tick occurred, so making a decision. Tick info: " + t + ", tick closing price: " + t.getClosePrice() + ", series end index: " + endInd);
		
		if (strategy.shouldEnter(endInd))
			decision = 1;
		else if (strategy.shouldExit(endInd))
			decision = 2;
		
		Logs.info("MovingStrategyTickManager.decideAfterTick(): decided to " + decision + " with close price " + t.getClosePrice() + ".");
		
		try
		{
			if (decision == 1)
			{
				OrderbookOrder actualOrder = broker.buy(t, forReal);
				boolean entered = previousActions.enter(endInd, Decimal.valueOf(actualOrder.getPricePerCoin()), Decimal.valueOf(actualOrder.getTotalPriceBTC()));
				if (entered)
					Logs.info("MovingStrategyTickManager.decideAfterTick(): trade history decided to " + decision + ".");
			}
			else if (decision == 2 && coinsHeld())
			{
				OrderbookOrder actualOrder = broker.sell(t, forReal);
				boolean exited = previousActions.exit(endInd, Decimal.valueOf(actualOrder.getPricePerCoin()), Decimal.valueOf(actualOrder.getTotalPriceBTC()));
				if (exited)
					Logs.info("MovingStrategyTickManager.decideAfterTick(): trade history decided to " + decision + ".");
			}
			
			if (decision != 0)
			{
				profitAnalyzer = new CashFlow(previousPrices, previousActions);
				Logs.debug("MovingStrategyTickManager.decideAfterTick(): new total profit: " + profitAnalyzer.getValue(profitAnalyzer.getSize() - 1));
			}
		}
		catch (Exception e) {
			throw new Exception("Error while executing broker action", e);
		}
	}
}
