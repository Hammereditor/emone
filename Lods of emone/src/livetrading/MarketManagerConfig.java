package livetrading;

import tradingobjs.Reinvestor;

public class MarketManagerConfig 
{
	private int marketID;
	private String cryptsy_apiKey_public;
	private String cryptsy_apiKey_private;
	private long broker_orderer_intervalBetweenChecks;
	private int broker_orderer_maxRetries;
	private int mstm_storedTicks;
	private int tickTimeS;
	private Reinvestor reinvestDecider;

	public MarketManagerConfig(int marketID, String cryptsy_apiKey_public, String cryptsy_apiKey_private, long broker_orderer_intervalBetweenChecks, int broker_orderer_maxRetries, int mstm_storedTicks, int tickTimeS, Reinvestor reinvestDecider)
	{
		this.marketID = marketID;
		this.cryptsy_apiKey_private = cryptsy_apiKey_private;
		this.cryptsy_apiKey_public = cryptsy_apiKey_public;
		this.broker_orderer_intervalBetweenChecks = broker_orderer_intervalBetweenChecks;
		this.broker_orderer_maxRetries = broker_orderer_maxRetries;
		this.mstm_storedTicks = mstm_storedTicks;
		this.tickTimeS = tickTimeS;
		this.reinvestDecider = reinvestDecider;
	}
	
	public int getMarketID() {
		return marketID;
	}

	public String getCryptsy_apiKey_public() {
		return cryptsy_apiKey_public;
	}

	public String getCryptsy_apiKey_private() {
		return cryptsy_apiKey_private;
	}

	public long getBroker_orderer_intervalBetweenChecks() {
		return broker_orderer_intervalBetweenChecks;
	}

	public int getBroker_orderer_maxRetries() {
		return broker_orderer_maxRetries;
	}

	public int getMstm_storedTicks() {
		return mstm_storedTicks;
	}
	
	public void setMstm_storedTicks(int t) {
		mstm_storedTicks = t;
	}

	public int getTickTimeS() {
		return tickTimeS;
	}
	
	public Reinvestor getReinvestor() {
		return reinvestDecider;
	}
}
