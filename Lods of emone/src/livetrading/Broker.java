package livetrading;

import config.Logs;
import tradingobjs.OrderbookOrder;
import tradingobjs.Reinvestor;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TradingRecord;

/**
 * Manages money and does transactions, based on the commands that the MovingStrategyTickManager object gives this
 * @author Hammereditor
 *
 */
public class Broker 
{
	//private TradingRecord previousActions;
	private Holdings balances;
	private Reinvestor reinvestDecider;
	
	private Orderer orderer;
	private long orderer_intervalBetweenChecks;
	private int orderer_maxRetries;
	
	public Broker(Cryptsy account, Reinvestor reinvestDecider, int marketID, long orderer_intervalBetweenChecks, int orderer_maxRetries) throws Exception
	{
		//this.previousActions = new TradingRecord();
		this.orderer_intervalBetweenChecks = orderer_intervalBetweenChecks;
		this.orderer_maxRetries = orderer_maxRetries;
		this.reinvestDecider = reinvestDecider;
		
		orderer = new Orderer(account, marketID);
		try { balances = new Holdings(account, marketID); } 
		catch (Exception e) {
			throw new Exception("Error while creating holding manager", e);
		}
	}
	
	/**
	 * Purchases coins and waits for the order to be completed
	 * @param t The most recent tick
	 * @return The Cryptsy orderID
	 * @throws Exception
	 */
	public OrderbookOrder buy(Tick t, boolean forReal) throws Exception
	{
		OrderbookOrder bestSellO = null;
		try { bestSellO = orderer.getBestSellOrder(); } catch (Exception e) {
			throw new Exception("Error while getting best sell order", e);
		}
		
		double buyPricePerCoin = bestSellO.getPricePerCoin();
		double buyVolume = bestSellO.getCoinQty();
		double btcBalance = balances.getBaseCurrencyBalance();
		double affordableBuyVolume = btcBalance / buyPricePerCoin; //without fee
		
		Logs.info("Broker.buy(): got best sell order to match. buyPricePerCoin: " + buyPricePerCoin + ", buyVolume: " + buyVolume + ", affordablebuyVolume: " + affordableBuyVolume);
		
		if (affordableBuyVolume < buyVolume)
			buyVolume = affordableBuyVolume;
		buyVolume = reinvestDecider.getReinvestVolume(buyVolume, buyPricePerCoin);
		
		OrderbookOrder obo = new OrderbookOrder(buyVolume, buyPricePerCoin);
		
		if (forReal)
		{
			long orderID = -1;
			try { orderID = orderer.buySync(buyVolume, buyPricePerCoin, orderer_intervalBetweenChecks, orderer_maxRetries); } catch (Exception e) {
				throw new Exception("Error while placing buy order", e);
			}
		}
		else
			Logs.info("Broker.buy(): order " + obo + " is not for real.");
		
		//previousActions.enter(index, price, amount)t.get
		return obo;
	}
	
	/**
	 * Sells coins and waits for the order to be completed
	 * @param t The most recent tick
	 * @return The Cryptsy orderID
	 * @throws Exception
	 */
	public OrderbookOrder sell(Tick t, boolean forReal) throws Exception
	{
		OrderbookOrder bestBuyO = null;
		try { bestBuyO = orderer.getBestBuyOrder(); } catch (Exception e) {
			throw new Exception("Error while getting best buy order", e);
		}
		
		double sellPricePerCoin = bestBuyO.getPricePerCoin();
		double sellVolume = bestBuyO.getCoinQty();
		double btcBalance = balances.getBaseCurrencyBalance();
		double affordableSellVolume = btcBalance / sellPricePerCoin; //without fee
		
		Logs.info("Broker.sell(): got best buy order to match. sellPricePerCoin: " + sellPricePerCoin + ", sellVolume: " + sellVolume + ", affordableSellVolume: " + affordableSellVolume);
		
		if (affordableSellVolume < sellVolume)
			sellVolume = affordableSellVolume;
		sellVolume = reinvestDecider.getReinvestVolume(sellVolume, sellPricePerCoin);
		
		OrderbookOrder obo = new OrderbookOrder(sellVolume, sellPricePerCoin);
		
		if (forReal)
		{
			long orderID = -1;
			try { orderID = orderer.sellSync(sellVolume, sellPricePerCoin, orderer_intervalBetweenChecks, orderer_maxRetries); } catch (Exception e) {
				throw new Exception("Error while placing sell order", e);
			}
		}
		else
			Logs.info("Broker.sell(): order " + obo + " is not for real.");
		
		return new OrderbookOrder(sellVolume, sellPricePerCoin);
	}
	
	public OrderbookOrder getBestBuyOrder() throws Exception
	{
		return orderer.getBestBuyOrder();
	}
	
	public OrderbookOrder getBestSellOrder() throws Exception
	{
		return orderer.getBestSellOrder();
	}
}
