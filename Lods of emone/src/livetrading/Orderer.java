package livetrading;

import eu.verdelhan.ta4j.Order;
import livetrading.Cryptsy.MarketOrderReturn;
import tradingobjs.OrderbookOrder;

/**
 * Executes transactions
 * @author Hammereditor
 *
 */
public class Orderer 
{
	private Cryptsy account;
	private int marketID;
	
	public Orderer(Cryptsy account, int marketID) //throws Exception
	{
		this.account = account;
		/*try { account.setAuthKeys(publicAPIkey, privateAPIkey); } catch (Exception e) {
			throw new Exception("Error while sending authentication", e);
		}*/
		
		this.marketID = marketID;
	}
	
	/**
	 * Purchases coins from the market, without waiting for completion
	 * @param coinQty
	 * @param pricePerCoin
	 * @return The Cryptsy order ID of the order
	 */
	public long buyAsync(double coinQty, double pricePerCoin) throws Exception
	{
		try { return account.createOrder(marketID, "Buy", coinQty, pricePerCoin); } catch (Exception e) {
			throw new Exception("Error while doing order", e);
		}
	}
	
	/**
	 * Sells coins to the market, without waiting for completion
	 * @param coinQty
	 * @param pricePerCoin
	 * @return The Cryptsy order ID of the order
	 */
	public long sellAsync(double coinQty, double pricePerCoin) throws Exception
	{
		try { return account.createOrder(marketID, "Sell", coinQty, pricePerCoin); } catch (Exception e) {
			throw new Exception("Error while doing order", e);
		}
	}
	
	/**
	 * Purchases coins from the market and checks for completion
	 * @param intervalBetweenChecks The time, in ms., to wait between order completion checks
	 * @param maxRetries The max. number of times to check. When this is exceeded, the order is considered "undoable", and canceled.
	 * @return The Cryptsy order ID of the order
	 */
	public long buySync(double coinQty, double pricePerCoin, long intervalBetweenChecks, int maxRetries) throws Exception
	{
		long orderID = buyAsync(coinQty, pricePerCoin);
		boolean bought = false;
		int attempt = 1;
		
		do
		{
			long time_start = System.currentTimeMillis();
			livetrading.Cryptsy.Order[] marketOrders = null;
			
			try { marketOrders = account.getMyOrders(marketID); } catch (Exception e) {
				throw new Exception("Error while getting account orders", e);
			}
			
			boolean found = false;
			for (int i = 0; i < marketOrders.length && !found; i++)
				if (marketOrders[i].orderid == orderID)
					found = true;
			
			bought = !found;
			attempt++;
			
			long waitTimeMs = intervalBetweenChecks - (System.currentTimeMillis() - time_start);
			if (waitTimeMs < 0)
				waitTimeMs = 0;
			try { Thread.sleep(waitTimeMs); } catch (Exception e) { }
		}
		while (!bought && attempt <= maxRetries);
		
		if (bought)
			return orderID;
		else
		{
			try { account.cancelOrder(orderID); } catch (Exception e) {
				throw new Exception("Error while canceling order " + orderID, e);
			}
			return -1;
		}
	}
	
	/**
	 * Sells coins to the market and checks for completion
	 * @param intervalBetweenChecks The time, in ms., to wait between order completion checks
	 * @param maxRetries The max. number of times to check. When this is exceeded, the order is considered "undoable", and canceled.
	 * @return The Cryptsy order ID of the order
	 */
	public long sellSync(double coinQty, double pricePerCoin, long intervalBetweenChecks, int maxRetries) throws Exception
	{
		long orderID = sellAsync(coinQty, pricePerCoin);
		boolean sold = false;
		int attempt = 1;
		
		do
		{
			long time_start = System.currentTimeMillis();
			livetrading.Cryptsy.Order[] marketOrders = null;
			
			try { marketOrders = account.getMyOrders(marketID); } catch (Exception e) {
				throw new Exception("Error while getting account orders", e);
			}
			
			boolean found = false;
			for (int i = 0; i < marketOrders.length && !found; i++)
				if (marketOrders[i].orderid == orderID)
					found = true;
			
			sold = !found;
			attempt++;
			
			long waitTimeMs = intervalBetweenChecks - (System.currentTimeMillis() - time_start);
			if (waitTimeMs < 0)
				waitTimeMs = 0;
			try { Thread.sleep(waitTimeMs); } catch (Exception e) { }
		}
		while (!sold && attempt <= maxRetries);
		
		if (sold)
			return orderID;
		else
		{
			try { account.cancelOrder(orderID); } catch (Exception e) {
				throw new Exception("Error while canceling order " + orderID, e);
			}
			return -1;
		}
	}
	
	
	/**
	 * Gets the lowest-priced sell order from the market
	 * @return The order
	 * @throws Exception
	 */
	public OrderbookOrder getBestSellOrder() throws Exception
	{
		MarketOrderReturn mor = null;
		
		try { mor = account.getMarketOrders(marketID); } catch (Exception e) {
			throw new Exception("Error while getting orderbook", e);
		}
		
		OrderbookOrder obo = new OrderbookOrder(mor.sellorders[0].quantity, mor.sellorders[0].sellprice);
		return obo;
	}
	
	/**
	 * Gets the highest-priced buy order from the market
	 * @return
	 * @throws Exception
	 */
	public OrderbookOrder getBestBuyOrder() throws Exception
	{
		MarketOrderReturn mor = null;
		
		try { mor = account.getMarketOrders(marketID); } catch (Exception e) {
			throw new Exception("Error while getting orderbook", e);
		}
		
		OrderbookOrder obo = new OrderbookOrder(mor.buyorders[0].quantity, mor.buyorders[0].buyprice);
		return obo;
	}
}
