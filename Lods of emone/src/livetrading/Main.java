package livetrading;

import livetrading.Cryptsy.Market;
import marketmanagers.BtcLtcManager;
import marketmanagers.BtcVtcManager;
import tradingobjs.Reinvestor;

public class Main 
{

	public static void main(String[] args) throws Exception
	{
		//start test DOGE/BTC market
		int marketID = 3;
		String cryptsy_apiKey_public = "688ce5ccc4e030aa49cd4e34e28d8a0b0f04a526";
		String cryptsy_apiKey_private = "f66506dd8eb11738a35307f130f1ee1e6e79d38d40ddd70108a875a1737d981852290dada1838b0d";
		long broker_orderer_intervalBetweenChecks = 1000;
		int broker_orderer_maxRetries = 5;
		int mstm_storedTicks = -1; //will be set later
		int tickTimeS = 900;
		Reinvestor reinvestDecider = new TestReinvestor();
		
		MarketManagerConfig testConfig = new MarketManagerConfig(marketID, cryptsy_apiKey_public, cryptsy_apiKey_private, broker_orderer_intervalBetweenChecks, broker_orderer_maxRetries, mstm_storedTicks, tickTimeS, reinvestDecider);
		MarketManager btcVtcManager = new BtcVtcManager(testConfig);
		Thread.sleep(Long.MAX_VALUE);
	}

}
