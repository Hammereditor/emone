package livetrading;

import java.util.List;

import com.sun.webkit.ThemeClient;

import cryptsyrecorder.DBquerier;
import tradingobjs.Trade;

/**
 * Provides the MovingStrategyTickManager with trades from the DB "trades" table
 * @author Hammereditor
 */
public class DatabaseMarketTickProvider extends MarketTickProvider 
{
	private int marketID;
	
	public DatabaseMarketTickProvider(MovingStrategyTickManager mstm, int tickIntervalS, int marketID) 
	{
		super(mstm, tickIntervalS);
		this.marketID = marketID;
	}

	protected List <Trade> getTrades(long startTimeS, long endTimeS) throws Exception 
	{
		List <Trade> trades = null;
		
		try
		{
			DBquerier dbq = new DBquerier(marketID);
			trades = dbq.getTrades(startTimeS, endTimeS);
		}
		catch (Exception e) {
			throw new Exception("Error while getting trades from DBquerier", e);
		}
		
		return trades;
	}
}
