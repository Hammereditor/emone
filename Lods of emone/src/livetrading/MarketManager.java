package livetrading;

import java.util.List;

import livetrading.Cryptsy.Market;
import tradingobjs.TimeSeriesTrades;
import tradingobjs.Trade;
import common.Stoppable;
import config.Logs;
import cryptsyrecorder.DBquerier;
import eu.verdelhan.ta4j.Strategy;
import eu.verdelhan.ta4j.TimeSeries;

/**
 * Manages all automatic trading for a market
 * @author Hammereditor
 *
 */
public abstract class MarketManager implements Stoppable
{
	private MarketManagerConfig config;
	//private MovingStrategyTickManager mstm;
	private MarketTickProvider mtp;
	private Thread mtpThread;
	
	public MarketManager(MarketManagerConfig config) throws Exception
	{
		Logs.debug("MarketManager[marketID:" + config.getMarketID() + "](): constructing...");
		this.config = config;
		config.setMstm_storedTicks(getStoredTicks());
		
		try
		{
			Cryptsy account = new Cryptsy();
			account.setAuthKeys(config.getCryptsy_apiKey_public(), config.getCryptsy_apiKey_private());
			Broker broke = new Broker(account, config.getReinvestor(), config.getMarketID(), config.getBroker_orderer_intervalBetweenChecks(), config.getBroker_orderer_maxRetries());
			Logs.debug("MarketManager[marketID:" + config.getMarketID() + "](): created account and broker objects.");
			
			TimeSeriesTrades pastTradeSeries = generateExistingSeries();
			MovingStrategyTickManager mstm = new MovingStrategyTickManager(generateStrategy(pastTradeSeries.getTimeSeries()), pastTradeSeries.getTimeSeries(), config.getMstm_storedTicks(), broke);
			Logs.debug("MarketManager[marketID:" + config.getMarketID() + "](): created MSTM object. Last " + config.getMstm_storedTicks() + " ticks: \n" + pastTradeSeries.getTimeSeries() + "");
			
			mtp = getMarketTickProvider(mstm, config);
			mtpThread = new Thread(mtp);
			mtpThread.start();
			Logs.debug("MarketManager[marketID:" + config.getMarketID() + "](): started market tick provider.");
		}
		catch (Exception e) {
			throw new Exception("Error while initializing objects", e);
		}
	}
	
	/**
	 * Generates or hard-codes the market tick provider, and constructs it with the given MSTM and configuration.
	 * @param mstm The tick manager
	 * @return
	 */
	protected abstract MarketTickProvider getMarketTickProvider(MovingStrategyTickManager mstm, MarketManagerConfig config);
	
	/**
	 * Based on the strategy, return the number of past ticks which the MSTM will store
	 */
	protected abstract int getStoredTicks();
	
	/**
	 * Generate or hard-code the market strategy, and return it
	 * @return The Strategy object
	 */
	protected abstract Strategy generateStrategy(TimeSeries pastSeries);
	
	/**
	 * Generates the series of past trades, so the MSTM will have previous prices to start off of
	 * @return
	 * @throws Exception
	 */
	private TimeSeriesTrades generateExistingSeries() throws Exception
	{
		List <Trade> pastTrades = null;
		long endTimeS = System.currentTimeMillis() / 1000;
		///////////////////////////////////
///////////////////////////////////
///////////////////////////////////
		//endTimeS -= (3600 * 10);
///////////////////////////////////
///////////////////////////////////
///////////////////////////////////
		long startTimeS = endTimeS - (config.getMstm_storedTicks() * config.getTickTimeS());
		Logs.debug("MarketManager[marketID:" + config.getMarketID() + "].generateExistingSeries(): getting trades from DB. Start: " + Logs.getFormattedDate(startTimeS) + " (" + startTimeS + ")" +
				", end: " + Logs.getFormattedDate(endTimeS) + " (" + endTimeS + "). Seconds: " + (endTimeS - startTimeS) + ".");
		
		try
		{
			DBquerier dbq = new DBquerier(config.getMarketID());
			pastTrades = dbq.getTrades(startTimeS, endTimeS);
		} catch (Exception e) {
			throw new Exception("Error while generating past time series", e);
		}
		
		Logs.debug("MarketManager[marketID:" + config.getMarketID() + "].generateExistingSeries(): past " + pastTrades.size() + " trades: " + pastTrades);
		return new TimeSeriesTrades(startTimeS * 1000, endTimeS * 1000, config.getTickTimeS(), pastTrades);
	}
	
	public void stop()
	{
		mtp.stop();
	}
}
