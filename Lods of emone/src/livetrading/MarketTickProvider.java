package livetrading;

import java.util.List;

import org.joda.time.DateTime;

import common.Stoppable;
import config.Logs;
import eu.verdelhan.ta4j.Tick;
import ta4jexamples.loaders.CsvTradesLoader;
import tradingobjs.Trade;

/**
 * Scans the market data, puts the new trades into a tick, and executes the action on the MovingStrategyTickManager
 * @author Hammereditor
 *
 */
public abstract class MarketTickProvider implements Runnable, Stoppable
{
	private MovingStrategyTickManager mstm;
	private int tickIntervalS; 
	private boolean stop;
	
	public MarketTickProvider(MovingStrategyTickManager mstm, int tickIntervalS)
	{
		this.mstm = mstm;
		this.tickIntervalS = tickIntervalS;
		this.stop = false;
		Logs.debug("MarketTickProvider(): constructed.");
	}
	
	@Override
	public void run()
	{
		int tickNum = 0;
		long whole_startTimeMs = System.currentTimeMillis(); // - (3600 * 1000 * 10);
		
		while (!stop)
		{
			long cycleTime_start = whole_startTimeMs + (tickNum * tickIntervalS * 1000); //System.currentTimeMillis() - (3600 * 1000 * 1);
			long cycleTime_start_S = cycleTime_start / 1000; //6000
			
			long startTimeS = cycleTime_start_S - tickIntervalS;
			long endTimeS = cycleTime_start_S;
			List <Trade> tradesDuringTick = null;
			
			Logs.debug("MarketTickProvider.run(): providing MSTM the next tick of data. Start time of data: " + Logs.getReadableTime(startTimeS) + ", end time of data: " + Logs.getReadableTime(endTimeS) + ".\n" +
			"Cycle start time: " + Logs.getReadableTime(cycleTime_start_S));
			
			try { tradesDuringTick = getTrades(startTimeS, endTimeS); } catch (Exception e) {
				Logs.error("MarketTickProvider.run(): error while getting trades: " + e.getMessage());
				Logs.printException(e);
				return;
			}
			
			if (tradesDuringTick.size() > 0)
			{
				Logs.debug("MarketTickProvider.run(): got " + tradesDuringTick.size() + " trades during the tick. Result: " + tradesDuringTick);
				
				double realOrderPrice = -1;
				try {
					realOrderPrice = mstm.getNextOrderPrice();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				Tick newT = storeTradesInNewTick(tradesDuringTick, startTimeS);
				double supposedOrderPrice = newT.getClosePrice().toDouble();
				
				if (Math.abs(supposedOrderPrice - realOrderPrice) / realOrderPrice > 0.01) //real order price is 1%+ worse
				{
					Logs.info("MarketTickProvider.run(): real price of " + realOrderPrice + " != supposed price of " + supposedOrderPrice + ", so not doing any decision for the cycle.");
				}
				else
				{
					newT = storeTradesInNewTick(tradesDuringTick, startTimeS);
					
					try { mstm.decideAfterTick(newT); } catch (Exception e) {
						Logs.error("MarketTickProvider.run(): error while making MSTM execute tick: " + e.getMessage());
						Logs.printException(e);
						return;
					}
				}
			}
			else
				Logs.info("MarketTickProvider.run(): not doing any decision for the cycle, because there are 0 trades.");
			
			long cycleTime_total = System.currentTimeMillis() - cycleTime_start; //(3600 * 1000 * 10) - cycleTime_start;
			long waitTime = (tickIntervalS * 1000) - cycleTime_total;
			Logs.debug("MarketTickProvider.run(): executed decision, and cycle complete. Waiting for " + waitTime + " ms....");
			tickNum++;
			try { Thread.sleep(waitTime); } catch (Exception e) { }
		}
	}
	
	/*@Override
	public void run()
	{
		while (!stop)
		{
			long cycleTime_start = System.currentTimeMillis();
			long cycleTime_start_S = cycleTime_start / 1000; //6000
			
			long startTimeS = cycleTime_start_S - tickIntervalS;
			long endTimeS = cycleTime_start_S;
			List <Trade> tradesDuringTick = null;
			
			Logs.debug("MarketTickProvider.run(): providing MSTM the next tick of data. Start time of data: " + Logs.getReadableTime(startTimeS) + ", end time of data: " + Logs.getReadableTime(endTimeS) + ".\n" +
			"Cycle start time: " + Logs.getReadableTime(cycleTime_start_S));
			
			try { tradesDuringTick = getTrades(startTimeS, endTimeS); } catch (Exception e) {
				Logs.error("MarketTickProvider.run(): error while getting trades: " + e.getMessage());
				Logs.printException(e);
				return;
			}
			
			if (tradesDuringTick.size() > 0)
			{
				Logs.debug("MarketTickProvider.run(): got trades during the tick. Result: " + tradesDuringTick);
				Tick newT = storeTradesInNewTick(tradesDuringTick, startTimeS);
				
				try { mstm.decideAfterTick(newT); } catch (Exception e) {
					Logs.error("MarketTickProvider.run(): error while making MSTM execute tick: " + e.getMessage());
					Logs.printException(e);
					return;
				}
			}
			else
				Logs.debug("MarketTickProvider.run(): not doing any decision for the cycle, because there are 0 trades.");
			
			long cycleTime_total = System.currentTimeMillis() - cycleTime_start;
			long waitTime = (tickIntervalS * 1000) - cycleTime_total;
			Logs.debug("MarketTickProvider.run(): executed decision, and cycle complete. Waiting for " + waitTime + " ms....");
			try { Thread.sleep(waitTime); } catch (Exception e) { }
		}
	}*/
		
	private Tick storeTradesInNewTick(List <Trade> trades, long startTimeS)
	{
		List <Tick> emptyTicks = CsvTradesLoader.buildEmptyTicks(new DateTime(startTimeS * 1000), new DateTime((startTimeS * 1000) + tickIntervalS), tickIntervalS);
		Tick t = emptyTicks.get(0);
		//Logs.debug("MarketTickProvider.storeTradesInNewTick(): trades: " + trades + ", start time: " + Logs.getReadableTime(startTimeS) + ", end time: " + (startTimeS + tickIntervalS) + ", empty ticks: " + emptyTicks.size());
		if (t == null) Logs.debug("MarketTickProvider.storeTradesInNewTick(): tick object is null.");
		Logs.debug("t: " + t.getTrades());
		
		for (Trade currT : trades)
		{
			t.addTrade(currT.getCoinQty(), currT.getPricePerCoin());
			//Logs.debug("MarketTickProvider.storeTradesInNewTick(): trade " + currT + " added to tick. tick closing price: " + t.getClosePrice());
		}
		return t;
	}
	
	private static List <Trade> makeTradePricesRealistic(List <Trade> trades, double amountToAdd)
	{
		for (int i = 0; i < trades.size(); i++)
		{
			Trade oldT = trades.get(i);
			Trade newT = new Trade(oldT.getMarketID(), oldT.getTradeID(), oldT.getTimestamp(), oldT.getOrderType(), oldT.getPricePerCoin() + amountToAdd, oldT.getCoinQty(), oldT.getTotalPriceBTC());
			trades.set(i, newT);
		}
		
		return trades;
	}
	
	public void stop()
	{
		stop = true;
	}

	protected abstract List <Trade> getTrades(long startTimeS, long endTimeS) throws Exception;
}
