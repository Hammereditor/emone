package common;

import java.util.ArrayList;
import java.util.List;

import configv2.Logs;

public class DBscheduler implements Runnable, Stoppable
{
	private List <DBaction> actions;
	private boolean stopped;
	private Object monitor;
	
	public DBscheduler()
	{
		actions = new ArrayList <DBaction> ();
		stopped = false;
		monitor = new Object();
	}
	
	public void addToQueue(DBaction action)
	{
		actions.add(action);
		synchronized (monitor)
		{
			monitor.notifyAll();
		}
	}
	
	public void stop()
	{
		stopped = true;
	}
	
	public void run()
	{
		while (!stopped)
		{
			synchronized (monitor)
			{
				while (actions.size() == 0)
				{
					//Logs.debug("DBquerier.run(): waiting for next action...");
					try { monitor.wait(); } catch (InterruptedException e) {}
				}
			}
			
			//Logs.debug("DBquerier.run(): Next action received.");
			DBaction nextAction = actions.remove(0);
			nextAction.executeDBaction();
		}
	}
}
