package common;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class Utils 
{
	public static boolean allSSLcertsTrusted = false;
	
	public static void setTrustAllCerts() throws Exception
	{
		TrustManager[] trustAllCerts = new TrustManager[]{
			new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted( java.security.cert.X509Certificate[] certs, String authType ) {	}
				public void checkServerTrusted( java.security.cert.X509Certificate[] certs, String authType ) {	}
			}
		};

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance( "SSL" );
			sc.init( null, trustAllCerts, new java.security.SecureRandom() );
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier( 
				new HostnameVerifier() {
					public boolean verify(String urlHostName, SSLSession session) {
						return true;
					}
				});
			
			allSSLcertsTrusted = true;
		}
		catch ( Exception e ) {
			throw new Exception("Error while installing manager", e);
		}
	}
	
	public static String getCryptsyAPIurl(String urlStr) throws Exception
	{
		StringBuilder sb = null;
		
		try
		{
			sb = new StringBuilder();
			URLConnection url = new URL(urlStr).openConnection();
			url.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0");
			BufferedReader netIn = new BufferedReader(new InputStreamReader(url.getInputStream()));
			
			String currL = null;
			while ((currL = netIn.readLine()) != null)
				sb.append(currL);
			
			netIn.close();
			return sb.toString();
		}
		catch (Exception e) {
			throw new Exception("Error while downloading market list", e);
		}
	}
	
	public static double valueToDouble(Object val)
	{
		double volumeBTC = 0.0;
		Object volumeBTCraw = val;
		if (volumeBTCraw instanceof Long)
			volumeBTC = ((long)volumeBTCraw) * 1.0;
		else
			volumeBTC = (double)volumeBTCraw;
		
		return volumeBTC;
	}
}
