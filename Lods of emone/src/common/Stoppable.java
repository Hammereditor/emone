package common;

public interface Stoppable
{
	public void stop();
}
