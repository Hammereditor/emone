package tradingobjs;

public class OrderbookOrder 
{
	private double coinQty, pricePerCoin, totalPriceBTC;

	public OrderbookOrder(double coinQty, double pricePerCoin)
	{
		this.coinQty = coinQty;
		this.pricePerCoin = pricePerCoin;
		totalPriceBTC = coinQty * pricePerCoin;
	}
	
	public double getCoinQty() {
		return coinQty;
	}

	public double getPricePerCoin() {
		return pricePerCoin;
	}

	public double getTotalPriceBTC() {
		return totalPriceBTC;
	}
	
	public String toString()
	{
		return "coinQty=" + coinQty + " pricePerCoin=" + pricePerCoin + " totalPriceBTC=" + totalPriceBTC;
	}
}
