package tradingobjs;

public class Market 
{
	private int marketID;
	private double volume24h;
	
	public Market(int marketID, double volume24h)
	{
		this.marketID = marketID;
		this.volume24h = volume24h;
	}
	
	public int getMarketID() {
		return marketID;
	}

	public double getVolume24h() {
		return volume24h;
	}
}
