package tradingobjs;

import config.Logs;

public class Trade extends OrderbookOrder implements Comparable<Trade>
{
	private int marketID;
	private long tradeID, timestamp;
	private String orderType;
	//private double pricePerCoin, coinQty, totalPriceBTC;
	
	public Trade(int marketID, long tradeID, long timestamp, String orderType, double pricePerCoin, double coinQty, double totalPriceBTC)
	{
		super(coinQty, pricePerCoin);
		this.marketID = marketID;
		this.tradeID = tradeID;
		this.timestamp = timestamp;
		this.orderType = orderType;
	}
	
	public int getMarketID() {
		return marketID;
	}

	public long getTradeID() {
		return tradeID;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public String getOrderType() {
		return orderType;
	}
	
	public String toString()
	{
		return "marketID=" + marketID + " tradeID=" + tradeID + " timestamp=" + timestamp + "(" + Logs.getReadableTime(timestamp) + ") orderType=" + orderType + " coinQty=" + getCoinQty() + " pricePerCoin=" + getPricePerCoin() + " totalPriceBTC=" + getTotalPriceBTC();
	}

	/**
	 * Compares two Trade objects based on the trade ID. Higher = newer trade, lower = older trade.
	 */
	public int compareTo(Trade arg0) 
	{
		return (int)(tradeID - arg0.getTradeID());
	}
}
