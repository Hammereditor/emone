package tradingobjs;

import java.util.Collections;
import java.util.List;

import org.joda.time.DateTime;

import ta4jexamples.loaders.CsvTradesLoader;
import eu.verdelhan.ta4j.Tick;
import eu.verdelhan.ta4j.TimeSeries;

/**
 * Creates a time-series of ticks based on past trades
 * @author Hammereditor
 */
public class TimeSeriesTrades 
{
	private TimeSeries timeS;
	
	/**
	 * Constructs a TimeSeriesTrades object based on an inputted list of trades and a tick time
	 * @param tickTimeS The interval length, in seconds
	 * @param trades The list of trades which happened during the time series
	 */
	public TimeSeriesTrades(long startTimeMs, long endTimeMs, int tickTimeS, List <Trade> trades)
	{
		List <Tick> ticks = CsvTradesLoader.buildEmptyTicks(new DateTime(startTimeMs), new DateTime(endTimeMs), tickTimeS);
		Collections.sort(trades);
		
		//fill the ticks with trades
		int tickInd = 0;
		int startTimeS = (int)(startTimeMs / 1000); //find out when the trade occurs, in seconds
		
		for (int i = 0; i < trades.size(); i++)
		{
			Trade currT = trades.get(i);
			int tradeTimeAfterStart = (int)(currT.getTimestamp() - startTimeS);
			
			int currTickStartTime = 0 + (tickInd * tickTimeS); //find out when the tick starts and ends
			int currTickEndTime = 0 + ((tickInd + 1) * tickTimeS);
			//System.out.println("trade time (S.) after start: " + tradeTimeAfterStart + ", currTickStartTime: " + currTickStartTime + ", currTickEndTime: " + currTickEndTime);
			
			while (!(tradeTimeAfterStart >= currTickStartTime && tradeTimeAfterStart < currTickEndTime)) //try the next tick until the trade's time fits into the tick
			{
				//System.out.println("\tCurrent trade is not within range. Incrementing tick...");
				tickInd++;
				currTickStartTime = 0 + (tickInd * tickTimeS); //re-calculate the starting and ending times with the next tick
				currTickEndTime = 0 + ((tickInd + 1) * tickTimeS);
				System.out.println("\ttrade time (S.) after start: " + tradeTimeAfterStart + ", currTickStartTime: " + currTickStartTime + ", currTickEndTime: " + currTickEndTime);
				
			}	
			
			//insert the trade into the tick
			ticks.get(tickInd).addTrade(currT.getCoinQty(), currT.getPricePerCoin());
		}
		
		//remove empty ticks
		CsvTradesLoader.removeEmptyTicks(ticks);
		timeS = new TimeSeries(ticks);
	}
	
	public TimeSeries getTimeSeries()
	{
		return timeS;
	}
}
