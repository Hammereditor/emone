package tradingobjs;

public abstract class Reinvestor 
{
	public static final double TRADINGFEE = 0.0027;
	
	public abstract double getReinvestVolume(double affordableVolumeWithoutFee, double pricePerCoin);
}
