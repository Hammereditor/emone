package cryptsyrecorder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import tradingobjs.Trade;
import common.Stoppable;
import common.Utils;
import config.Logs;

public class MarketRecorder implements Runnable, Stoppable
{
	private int cycleTimeMs, marketID;
	private boolean stopped;
	private List <Trade> previouslyInsertedTrades;
	
	public MarketRecorder(int cycleTimeMs, int marketID)
	{
		this.cycleTimeMs = cycleTimeMs;
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
		//if (marketID == 132)
			//this.cycleTimeMs = 10000;
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
		this.marketID = marketID;
		stopped = false;
	}
	
	public void run()
	{
		while (!stopped)
		{
			try 
			{ 
				Logs.info("MarketRecorder[marketID:" + marketID + "].run(): doing cycle...");
				List <Trade> trades = getAPItrades();
				DBquerier dbq = new DBquerier(marketID);
				
				if (previouslyInsertedTrades != null)
					dbq.storeOnlyNewTrades(trades, previouslyInsertedTrades);
				else
				{
					dbq.deleteTradesByID(trades); //make sure duplicate trade rows aren't inserted
					dbq = new DBquerier(marketID);
					dbq.storeTrades(trades);
				}
				
				previouslyInsertedTrades = trades;
				//dbq.disconnect();
				Logs.info("MarketRecorder[marketID:" + marketID + "].run(): doing cycle...");
			}
			catch (Exception e) {
				System.out.println("Recorder[marketID:" + marketID + "].run(): error while doing cycle: " + e.getMessage());
				e.printStackTrace();
			}
			
			try { Thread.sleep(cycleTimeMs); } catch (Exception e) {}
		}
	}
	
	public void stop()
	{
		stopped = true;
	}
	
	private List <Trade> getAPItrades() throws Exception	
	{
		String urlStr = "https://api.cryptsy.com/api/v2/markets/" + marketID + "/tradehistory?limit=100";
		String json = null;
		
		try
		{
			json = Utils.getCryptsyAPIurl(urlStr);
		} catch (Exception e) {
			throw new Exception("Error while downloading trade history", e);
		}
		
		List <Trade> trades = new ArrayList <Trade> ();
		
		try
		{
			JSONParser p = new JSONParser();
			JSONObject rootObj = (JSONObject)p.parse(json);
			JSONArray dataArray = (JSONArray)rootObj.get("data");
			
			for (int i = 0; i < dataArray.size(); i++)
			{
				JSONObject currTradeObj = (JSONObject)dataArray.get(i);
				//System.out.println(currTradeObj.toJSONString());
				long tradeID = Long.parseLong((String)currTradeObj.get("tradeid")), timestamp = (long)currTradeObj.get("timestamp");
				String orderType = (String)currTradeObj.get("initiate_ordertype");
				
				double pricePerCoin = Utils.valueToDouble(currTradeObj.get("tradeprice")), 
						coinQty = Utils.valueToDouble(currTradeObj.get("quantity")), 
						totalPriceBTC = Utils.valueToDouble(currTradeObj.get("total"));
				
				Trade t = new Trade(marketID, tradeID, timestamp, orderType, pricePerCoin, coinQty, totalPriceBTC);
				trades.add(t);
			}
		}
		catch (Exception e) {
			throw new Exception("Error while parsing JSON trades", e);
		}
		
		return trades;
	}
}
