package cryptsyrecorder;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import tradingobjs.Trade;
import common.DBaction;
import common.DBscheduler;
import config.Config;
import config.Logs;

public class DBquerier 
{
	private Connection dbConn;
	private int marketID;
	
	/**
	 * Creates a new DBquerier and connects
	 * @throws Exception If there was an error connecting to the database
	 */
	public DBquerier(int marketID) throws Exception
	{
		dbConn = null;
		this.marketID = marketID;
		connect();
	}
	
	/**
	 * Connects to the database
	 * @throws Exception If there was a database error
	 */
	private void connect() throws Exception
	{
		try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            throw new Exception("DB driver not found", e);
        }
		dbConn = DriverManager.getConnection("jdbc:mysql://" + Config.db_host_ipAddr + ":" + Config.db_host_port + "/" + Config.db_schemaName, Config.db_username, Config.db_password);
	}
	
	/**
	 * Disconnects from the database
	 * @throws IllegalStateException If the object is not connected yet
	 * @throws SQLException If there was an error disconnecting
	 */
	public void disconnect() throws IllegalStateException, SQLException
	{
		if (dbConn == null)
			throw new IllegalStateException("Not connected yet, so cannot disconnect");
		dbConn.close();
	}
	
	public void deleteTradesByID(List <Trade> trades) throws Exception
	{
		//int marketID = trades.get(0).getMarketID();
		StringBuilder stmtStr = new StringBuilder("DELETE FROM trades WHERE MarketID = " + marketID + " AND (");
		
		for (int i = 0; i < trades.size(); i++)
		{
			Trade currT = trades.get(i);
			stmtStr.append("TradeID = " + currT.getTradeID());
			
			if (i + 1 < trades.size())
				stmtStr.append(" OR ");
		}
		
		stmtStr.append(");");
		
		try
		{
			/*Logs.debug("DBquerier[marketID:" + marketID + "].deleteTradesByID(): waiting until DB unlocked...");
			DBscheduler.waitUntilUnlocked();
			Logs.debug("DBquerier[marketID:" + marketID + "].deleteTradesByID(): locking DB...");
			DBscheduler.lock();*/
			final String stmtStrF = stmtStr.toString();
			
			Recorder.scheduler.addToQueue(new DBaction()
			{
				public void executeDBaction()
				{
					try
					{
						Statement stmt = dbConn.createStatement();
						stmt.executeUpdate(stmtStrF);
						stmt.close();
						disconnect();
					}
					catch (Exception e) {
						Logs.error("DBquerier[marketID:" + marketID + "].deleteTradesByID(): error while executing DBaction: " + e.getMessage());
						Logs.printException(e);
					}
				}
			});
			
			//Logs.debug("DBquerier[marketID:" + marketID + "].deleteTradesByID(): done with query.");
		} 
		catch (Exception e) {
			throw new Exception("Error while sending statement", e);
		}
	}
	
	public void storeTrades(List <Trade> trades) throws Exception
	{
		if (trades.size() > 0)
		{
			StringBuilder stmtStr = new StringBuilder("INSERT INTO trades VALUES ");
			
			for (int i = 0; i < trades.size(); i++)
			{
				Trade currT = trades.get(i);
				stmtStr.append("(" + currT.getMarketID() + ", " + currT.getTradeID() + ", " + currT.getTimestamp() + ", \'" + currT.getOrderType() + "\', " + currT.getPricePerCoin() + ", " +
						currT.getCoinQty() + ", " + currT.getTotalPriceBTC() + ")");
				
				if (i + 1 < trades.size())
					stmtStr.append(", ");
			}
			
			stmtStr.append(";");
			
			try
			{
				/*Logs.debug("DBquerier[marketID:" + marketID + "].storeTrades(): waiting until DB unlocked...");
				DBscheduler.waitUntilUnlocked();
				Logs.debug("DBquerier[marketID:" + marketID + "].storeTrades(): locking DB...");
				DBscheduler.lock();*/
				final String stmtStrF = stmtStr.toString();
				
				Recorder.scheduler.addToQueue(new DBaction()
				{
					public void executeDBaction()
					{
						try
						{
							Statement stmt = dbConn.createStatement();
							stmt.executeUpdate(stmtStrF);
							stmt.close();
							disconnect();
						}
						catch (Exception e) {
							Logs.error("DBquerier[marketID:" + marketID + "].storeTrades(): error while executing DBaction: " + e.getMessage());
							Logs.printException(e);
						}
					}
				});
				
				//Logs.debug("DBquerier[marketID:" + marketID + "].storeTrades(): done with query.");
			} 
			catch (Exception e) {
				Logs.error("DBquerier[marketID:" + marketID + "].storeTrades(): " + stmtStr.toString());
				throw new Exception("Error while sending statement", e);
			}
		}
	}
	
	public void storeOnlyNewTrades(List <Trade> trades, List <Trade> previouslyInsertedTrades) throws Exception
	{
		Trade newestPreviousTrade = previouslyInsertedTrades.get(0);
		
		for (Trade previousT : previouslyInsertedTrades)
			if (previousT.getTradeID() > newestPreviousTrade.getTradeID())
				newestPreviousTrade = previousT;
		
		List <Trade> newTrades = new ArrayList <Trade> ();
		
		for (Trade t : trades)
			if (t.getTradeID() > newestPreviousTrade.getTradeID())
				newTrades.add(t);
		
		try { storeTrades(newTrades); } catch (Exception e) {
			throw new Exception("Error while inserting list of new trades", e);
		}
	}
	
	public List <Trade> getTrades(long startTimeS, long endTimeS) throws Exception
	{
		List <Trade> trades = new ArrayList <Trade> ();
		
		try
		{
			final String stmtStrF = "SELECT * FROM trades WHERE MarketID = " + marketID + " AND Timestamp >= " + startTimeS + " AND Timestamp <= " + endTimeS + ";";
			Statement stmt = dbConn.createStatement();
			ResultSet res = stmt.executeQuery(stmtStrF);
			
			while (res.next())
			{
				long tradeID = res.getLong("TradeID");
				long timestamp = res.getLong("Timestamp");
				String orderType = res.getString("OrderType");
				double pricePerCoin = res.getDouble("PricePerCoin");
				double coinQty = res.getDouble("CoinQty");
				double totalPriceBTC = res.getDouble("TotalPriceBTC");
				
				Trade t = new Trade(marketID, tradeID, timestamp, orderType, pricePerCoin, coinQty, totalPriceBTC);
				trades.add(t);
			}
			
			res.close();
			stmt.close();
			disconnect();
		}
		catch (Exception e) {
			throw new Exception("Error while querying DB", e);
		}
		
		return trades;
	}
}
