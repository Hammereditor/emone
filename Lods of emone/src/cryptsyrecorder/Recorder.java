package cryptsyrecorder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import tradingobjs.Market;
import common.DBscheduler;
import common.Stoppable;
import common.Utils;
import config.Config;

public class Recorder implements Stoppable
{
	private Thread[] marketRthreads;
	private Thread dbsT;
	public static DBscheduler scheduler;
	
	public Recorder(boolean speedUpDoge) throws Exception
	{
		scheduler = new DBscheduler();
		dbsT = new Thread(scheduler);
		dbsT.start();
		
		List <Market> markets = new ArrayList <Market> ();
		try { markets = getMarkets(); } catch (Exception e) {
			throw new Exception("Error while getting markets", e);
		}
		
		for (int i = markets.size() - 1; i >= 0; i--)
		{
			Market currM = markets.get(i);
			//System.out.println(i + " " + currM.getVolume24h());
			if (currM.getVolume24h() < Config.main_min24hourBTCvolume)
				markets.remove(i);
		}
		
		//start market recorders
		System.out.println("f" + markets.size());
		marketRthreads = new Thread[markets.size()];
		
		for (int i = 0; i < markets.size(); i++)
		{
			int cycleTimeMs = Config.recorder_cycleTimeMs;
			if (speedUpDoge && markets.get(i).getMarketID() == 132)
				cycleTimeMs = 10000;
			
			MarketRecorder rec = new MarketRecorder(cycleTimeMs, markets.get(i).getMarketID());
			//System.out.println("k");
			Thread t = new Thread(rec);
			marketRthreads[i] = t;
			//System.out.println("k");
			t.start();
			//System.out.println("k");
		}
	}
	
	private List <Market> getMarkets() throws Exception
	{
		String urlStr = "https://api.cryptsy.com/api/v2/markets";
		String json = null;
		
		try
		{
			json = Utils.getCryptsyAPIurl(urlStr);
		}
		catch (Exception e) {
			throw new Exception("Error while downloading market list", e);
		}
		
		List <Market> markets = new ArrayList <Market> ();
		
		try
		{
			JSONParser p = new JSONParser();
			JSONObject rootObj = (JSONObject)p.parse(json);
			JSONArray dataArray = (JSONArray)rootObj.get("data");
			
			for (int i = 0; i < dataArray.size(); i++)
			{
				JSONObject currMarketObj = (JSONObject)dataArray.get(i);
				//System.out.println(currMarketObj.toJSONString());
				int marketID = Integer.parseInt((String)currMarketObj.get("id"));
				JSONObject hour24obj = (JSONObject)currMarketObj.get("24hr");
				double volumeBTC = Utils.valueToDouble(hour24obj.get("volume_btc"));
				
				Market m = new Market(marketID, volumeBTC);
				markets.add(m);
			}
			
			return markets;
		}
		catch (Exception e) {
			throw new Exception("Error while parsing market list", e);
		}	
	}

	public void stop() 
	{
		for (Thread t : marketRthreads)
			t.stop();
		dbsT.stop();
	}
}
